package tk.stegoleopluradon.logtocharcoal;

import java.util.ArrayList;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.plugin.java.JavaPlugin;

public class Main extends JavaPlugin {
	public ArrayList<Material> allowedLogs = new ArrayList<Material>();

	public void setup() {
		allowedLogs.add(Material.ACACIA_LOG);
		allowedLogs.add(Material.BIRCH_LOG);
		allowedLogs.add(Material.SPRUCE_LOG);
		allowedLogs.add(Material.OAK_LOG);
		allowedLogs.add(Material.DARK_OAK_LOG);
		allowedLogs.add(Material.JUNGLE_LOG);
		allowedLogs.add(Material.STRIPPED_ACACIA_LOG);
		allowedLogs.add(Material.STRIPPED_BIRCH_LOG);
		allowedLogs.add(Material.STRIPPED_SPRUCE_LOG);
		allowedLogs.add(Material.STRIPPED_OAK_LOG);
		allowedLogs.add(Material.STRIPPED_DARK_OAK_LOG);
		allowedLogs.add(Material.STRIPPED_JUNGLE_LOG);
	}

	@Override
	public void onEnable() {
		setup();
		Bukkit.getPluginManager().registerEvents(new blockBurn(allowedLogs), this);
	}

	@Override
	public void onDisable() {
	}

}
