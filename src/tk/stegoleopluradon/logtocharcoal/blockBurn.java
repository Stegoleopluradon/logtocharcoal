package tk.stegoleopluradon.logtocharcoal;

import java.util.ArrayList;
import java.util.Random;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.World;
import org.bukkit.entity.Item;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBurnEvent;

import org.bukkit.inventory.ItemStack;
import org.bukkit.util.Vector;



public class blockBurn implements Listener {
	ArrayList<Material> allowedLogs;

	public blockBurn(ArrayList<Material> allowedLogs) {
		this.allowedLogs = allowedLogs;
	}

	@EventHandler
	public void onBlockBurn(BlockBurnEvent e) {
		Material b1 = e.getBlock().getType();
		World w = e.getBlock().getWorld();
		if (allowedLogs.contains(b1)) {
			Random rand = new Random();
			int number = rand.nextInt(10) + 1;
			if (number <= 6) {
				ItemStack itemStack = new ItemStack(Material.CHARCOAL);
				Location location = e.getBlock().getLocation();
				location.setY(e.getBlock().getY() + rand.nextInt(4));
				location.setX(e.getBlock().getX() + rand.nextInt(3));
				location.setZ(e.getBlock().getZ() + rand.nextInt(3));
				Item item = w.dropItemNaturally(location, itemStack);
				
				item.setVelocity(new Vector(randomNegativeInt()*(rand.nextDouble()/2)-0.1,0,randomNegativeInt()*(rand.nextDouble()/2)-0.1));
				w.playSound(location, Sound.ENTITY_FIREWORK_ROCKET_BLAST, 1.0f, 1.5f);

			}
		}

	}
	public int randomNegativeInt() {
		return ((new Random().nextInt(1) > 0) ? 1 : -1);
	}

}
